## OpenShift CI/CD and GitOps

This section should help you to get started with Helm Operations, referred to as HelmOps and CI/CD with OpenShift Pipelines known also as Tekton running on your Laptop or any other OpenShift Cluster.

We’ll be also using GitLab to store the released Helm Chart and deploy it from there through OpenShift GitOps in the last section of this README.

## Helm Chart Continuous Integration with OpenShift Pipelines

Just like every other part of the development process, Helm Charts development must ensure quality standards in order to avoid issues related to misconfiguration or security vulnerabilities. We can handle this process in a number of different ways, starting from static checks against predefined rules based on best practices and mandatory requirements, followed by custom checks implemented by the Dev/Ops team(s) to meet specific requirements.

#### Requirements

- You've already initialized the Helm Chart with some OpenShift/Kubernetes manifests and pushed them to GitLab
- Access to Vault with with some required secrets stored into it (see the next section for more details)
- Access to an OpenShift Cluster with persistent storage
- Tekton Pipelines installed in the OpenShift CI platform
- External Secrets Operator installed in the OpenShift CI platform
- Admin access to a CI namespace where the CI Pipeline will be created
- Optional: podman, buildah and tekton CLI installed locally

#### Secrets Requirements and Management

The following secrets are required for the CI Pipeline:
  - `Gitlab Credentials`: Access token to checkout the helm git repo and push the chart package to the Gitlab registry after build
  - `GPG Secret Key`: An existing GPG secret key to sign the Helm Chart, or you can [generating a new one](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key). Helm currently prefers the older format of GPG keys. You have to convert the GPG secret keyring format to the old format and encode it `Base64` before storing it vault:

```
(dev)$ gpg --export 'Foo Bar' >~/.gnupg/pubring.gpg
(dev)$ gpg --export-secret-keys 'Foo BAr' >~/.gnupg/secring.gpg

(dev)$ base64 --wrap=0 ~/.gnupg/secring.gpg
(dev)$ base64 --wrap=0 ~/.gnupg/secring.gpg
```

Now, first make sure the following Vault requirements are correctluy set up.

1. Make sure you've access to the Vault via CLI and UI (vault client CLI is required)

```
(dev)$ export VAULT_ADDR=http(s)://<vault-addr>:<vault-port>
(dev)$ export VAULT_TOKEN=<vault-token>
(dev)$ vault status
```

2. Make sure the following secrets are set or create them in the Vault as follow:

```
# Gitlab credentials to access the Helm Chart git repository
secret/gitops/ci/helm-ops/git-access-creds
{
  "password": "<gitlab-token>",
  "username": "<gitlab-username>"
}

# GPG keys for signing and verifying helm charts
secret/common/helm-sign-gpg
{
  "public-key": "<gpg-public-key>",
  "secret-key": "<gpg-secret-key>"
}
```

3. Enable the AppRole authentication method

```
$ vault auth enable approle
```

1. Create an `AppRole` to be used by `External Secrets Operator` to pull secrets from Vault with ReadOnly access:

```
vi helm-ci-policy.hcl

path "secret/data/gitops/ci/helm-ops/*" {
  capabilities = ["read"]
}

path "secret/data/common/helm-sign-gpg" {
  capabilities = ["read"]
}
```

```
$ vault policy write helm-ci-policy ./helm-ci-policy.hcl

$ vault write auth/approle/role/helm-ci token_policies="helm-ci-policy"
```

5. Fetch the `RoleID` of the AppRole

```
$ vault read auth/approle/role/helm-ci/role-id

role_id     xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx
```

6. Get the `SecretID` issued against the AppRole

```
$ vault write -f auth/approle/role/helm-ci/secret-id

secret_id       xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx
[...]
```

**PS:**

- The `RoleID` is equivalent to a username, and SecretID is the corresponding password. The External Secrets Operator (ESO) needs both to log in with Vault. These values are confidential and must be provided to the ESO in a secure way.

- For security reasons, the SecretID is configured to be regenerated after a certain number of use (40 times by default). To change this, you can update the role definition with `secret_id_num_uses` set to another value. 

Now, configure the External Secrets Operator integration with HashiCorp Vault for secret management.

1. Login to the OCP Cluster:

```bash
(dev)$ oc login --token=<ocp-oatuh-token> --server=https://<ocp-api>:6443

(dev)$ oc project <your-ci-namespace>
```

2. Create a Secret that stores the Vault AppRole SecretId

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: vault-approle-secret
type: Opaque
stringData:
  secret-id: xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx
```

3. Create a `SecretStore` Custom Resource:

```yaml
apiVersion: external-secrets.io/v1beta1
kind: SecretStore
metadata:
  name: vault-backend
spec:
  refreshInterval: 3600
  provider:
    vault:
      server: "http://vault.vault.svc.cluster.local:8200"
      path: "secret"
      version: "v2"
      auth:
        # Vault AppRole authentication
        appRole:
          # Path where the AppRole authentication backend is mounted
          path: "approle"
          # RoleID configured in the AppRole authentication backend
          roleId: "xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx"
          # Reference to a key in a K8 Secret that contains the App Role SecretId
          secretRef:
            name: "vault-approle-secret"
            key: "secret-id"
```

The AppRole authentication reads the SecretID from a Kind=Secret and uses the specified RoleID to aquire a temporary token to fetch secrets.

4. Make sure the SecretStore is in a Ready state

```
$ oc get secretstore

NAME            AGE   STATUS   CAPABILITIES   READY
vault-backend   22s   Valid    ReadWrite      True
```

**PS:** In case of a `ClusterSecretStore`, Be sure to provide namespace in secretRef with the namespace where the secret resides.


#### Build a Helm base image

In this section we'll be describing how to build a Helm base image that will be used later in the Tekton CI Pipeline to build and release the OpenShift Day2Ops Helm Chart.

1. Clone the OpenShift GitOps git repo

2. Check and adapt if required the Helm `Containerfile` under `./pipelines/base-images/helm/`

3. (Option 1): You can build the image locally and push it to the OpenShift cluster

Login to the OCP Cluster :

```bash
(dev)$ oc login --token=<ocp-oatuh-token> --server=https://<ocp-api>:6443

(dev)$ oc project <your-ci-namespace>

(dev)$ oc registry login
```

Build the image:

```bash
(dev)$ oc registry info

(dev)$ buildah bud -t <registry-hostname>/<your-ci-namespace>/helm:latest -f Containerfile ./pipelines/base-images/helm/

(dev)$ buildah images

REPOSITORY                                              TAG          IMAGE ID       CREATED          SIZE
<registry-hostname>/<your-ci-namespace>/helm            latest       aedae268abe7   28 seconds ago   457 MB
```

Push the image to the OpenShift Registry:

```bash
(dev)$ export DOCKER_CONFIG=~/.docker/

(dev)$ buildah push --tls-verify=false <registry-hostname>/<your-ci-namespace>/helm:latest
```

**Warning:** the default reading order of registry auth file will be changed from `${HOME}/.docker/config.json` to podman registry config locations in the future version of `oc`. `${HOME}/.docker/config.json` is deprecated, but can still be used for storing credentials as a fallback. See https://github.com/containers/image/blob/main/docs/containers-auth.json.5.md for the order of podman registry config locations.

4. (Option 2): You can build the image directly in the OpenShift cluster

Create an `ImageStream` and a `BuildConfig`:

```bash
(dev)$ oc project <your-ci-namespace>

(dev)$ oc apply -f ./pipelines/base-images/helm/imagestream.yaml

(dev)$ oc apply -f ./pipelines/base-images/helm/buildconfig.yaml
```

A build is automatically started. Wait until it finishes then chech the built image:

```bash
(dev)$ oc get builds

(dev)$ oc logs -f build/build-helm-image-1

(dev)$ oc get istag
```

#### Create a CI Pipeline

1. Make sure you switch to your CI namespace

```bash
(dev)$ oc project <your-ci-namespace>
```

2. Create the Pipeline Tasks:

```bash
(dev)$ oc apply -f ./pipelines/ci/tasks/
```

3. Create the CI Pipeline:

```bash
(dev)$ oc apply -f ./pipelines/ci/helm-chart-ci-pipeline.yaml
```

4. Check the created pipeline

```bash
(dev)$ tkn pipeline list
```

#### Prepare and run the CI Pipeline

1. Login to the OCP Cluster:

```bash
(dev)$ oc login --token=<ocp-oatuh-token> --server=https://<ocp-api>:6443

(dev)$ oc project <your-ci-namespace>
```

2. Create a PVC

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: helm-workspace
  labels:
    app: helm-ci
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 1Gi
```

3. Create the following `ExternalSecrets`

To access Git with a Token:

```yaml
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: vault-git-access-creds
  labels:
    app: helm-ci
spec:
  refreshInterval: "1h"
  secretStoreRef:
    name: vault-backend
    kind: SecretStore
  # The kubernetes secret resource to be create
  target:
    name: git-access-creds
    template:
      type: kubernetes.io/basic-auth
      metadata:
        annotations:
          tekton.dev/git-0: 'https://gitlab.com'
  data:
    # The key name inside the Kubernetes secret object
  - secretKey: username
    remoteRef:
      # The path to the vault's secret name
      key: gitops/ci/helm-ops/git-access-creds
      # The property's name inside the vault's secret
      property: username
  - secretKey: password
    remoteRef:
      key: gitops/ci/helm-ops/git-access-creds
      property: password
```

and to sign the Helm chart with a GPG key:

```yaml
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: vault-helm-sign-gpg
  labels:
    app: helm-ci
spec:
  refreshInterval: "1h"
  secretStoreRef:
    name: vault-backend
    kind: SecretStore
  # The kubernetes secret resource to create
  target:
    name: helm-sign-gpg
  data:
    # The key name inside the Kubernetes secret object
  - secretKey: public-key
    remoteRef:
      # The path to the vault's secret name
      key: common/helm-sign-gpg
      # The property's name inside the vault's secret
      property: public-key
      decodingStrategy: Base64
  - secretKey: secret-key
    remoteRef:
      key: common/helm-sign-gpg
      property: secret-key
      decodingStrategy: Base64
```

**PS:**:
  - Fetching the labels with `metadataPolicy: Fetch` only works with Vault KV sercrets Engine version v2.
  - The GPG key is a binary data encoded with base64 in order to be stored into Vault. With the `decodingStrategy: Base64` ESO will try to decode the secret value using base64 method and create a Kubernetes Secret object for it.

4. Check the created secrets

```bash
(dev)$ oc get secrets git-access-creds helm-sign-gpg
```

5. Attach the git secret to the `pipeline` service account

```bash
(dev)$ oc secrets link pipeline git-access-creds
```

6. Get the Helm Image reference from the local OpenShift registry

```bash
(dev)$ oc get istag helm:latest -o template='{{ .image.dockerImageReference }}'
```

7. Start the Pipeline

```bash
(dev)$ tkn pipeline start openshift-day2ops-helm-chart-ci-pipeline \
        --param git-branch="<git-branch-name>" \
        --param git-repo-url="https://<gitlab-hostname>/<organization-name>/<git-project-repo>.git" \
        --param ocp-target-version="<openshift-target-cluster-version>" \
        --param helm-image="<image-reference>" \
        --param pluto-args="--output normal" \
        --param gitlab-registry-url="https://<gitlab-hostname>/api/v4/projects/<project-id>/packages/helm/<channel>" \
        --workspace name=helm-chart-git-repo,claimName=helm-workspace \
        --workspace name=helm-sign-secret,secret=helm-sign-gpg

PipelineRun started: openshift-day2ops-helm-chart-ci-pipeline-run-xxxxx
va
In order to track the PipelineRun progress run:
tkn pipelinerun logs openshift-day2ops-helm-chart-ci-pipeline-run-xxxx -f
```

**PS:**

- The image reference built (following in the above steps) should be something like: `image-registry.openshift-image-registry.svc:5000/<your-ci-namespace>/helm@sha256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`
- In order to push the Helm Chart to a Gitlab registry, make sure the Registry URL format is as follow: `https://<gitlab-hostname>/api/v4/projects/<project-id>/packages/helm/<channel>`
- The `channel` in the gitlab Helm repository url could be `devel` or `stable` depending on the Helm chart release maturity.

See the `Resources` section for more details.

#### Clean UP

```
$ oc project <your-ci-namespace>
$ oc delete pipelinerun -l app=helm-ci
$ oc delete pipelines -l app=helm-ci
$ oc delete tasks -l app=helm-ci
$ oc secrets unlink pipeline git-access-creds
$ oc delete externalsecret -l app=helm-ci
$ oc delete secretstore vault-backend
$ oc delete pvc -l app=helm-ci
```

## Helm Chart Continuous Deployment with OpenShift GitOps

TODO

## Resources

- [How to create a Helm Chart](https://devopscube.com/create-helm-chart/)
- [Installing the Red Hat OpenShift Pipelines CLI on Linux](https://docs.openshift.com/container-platform/4.11/cli_reference/tkn_cli/installing-tkn.html)
- [Helm charts in the Gitlab Package Registry](https://docs.gitlab.com/ee/user/packages/helm_repository/)
- [RedHat Developer - Tekton Tutorial](https://redhat-scholars.github.io/tekton-tutorial/tekton-tutorial/)
- [Helm best practices](https://codefresh.io/docs/docs/ci-cd-guides/helm-best-practices/)
- [Helm Chart Testing Tools Overview](https://cloudentity.com/developers/blog/helm_chart_testing_tools)
- [Open Policy Agent for Kubernetes](https://www.openpolicyagent.org/docs/latest/kubernetes-introduction/)
- [AppRole Auth Method](https://developer.hashicorp.com/vault/docs/auth/approle#authentication)
- [AppRole Pull Authentication](https://developer.hashicorp.com/vault/tutorials/auth-methods/approle)
- [Package, sign and verify charts](https://kodekloud.com/blog/package-sign-and-verify-charts/)
