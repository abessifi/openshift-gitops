# Base image: https://catalog.redhat.com/software/containers/ubi8/ubi/5c359854d70cc534b3a3784e?container-tabs=overview
FROM registry.access.redhat.com/ubi8/ubi

# Install some required packages

RUN yum install openssl git jq wget -y && \
    wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 && \
    chmod +x /usr/local/bin/yq

# Install Helm and the ChartMuseum Plugin
# https://helm.sh/docs/intro/install/#from-script
# https://github.com/chartmuseum/helm-push

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh && \
    helm plugin install https://github.com/chartmuseum/helm-push && \
    echo "Helm: $(helm version)"

# Install Pluto
# https://pluto.docs.fairwinds.com/

RUN curl -s https://api.github.com/repos/FairwindsOps/pluto/releases/latest | jq '.assets[] | select(.name|match("linux_amd64.tar.gz$")) | .browser_download_url' | xargs -n 1 curl -s -O -L && \
    tar -xf pluto_*_linux_amd64.tar.gz && \
    mv pluto /usr/local/bin/ && \
    echo "Pluto: $(pluto version)"

# Install OPA Conftest
RUN export LATEST_VERSION=$(wget -q -O - "https://api.github.com/repos/open-policy-agent/conftest/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 2-) && \
    wget "https://github.com/open-policy-agent/conftest/releases/download/v${LATEST_VERSION}/conftest_${LATEST_VERSION}_Linux_x86_64.tar.gz" && \
    tar xzf conftest_${LATEST_VERSION}_Linux_x86_64.tar.gz && \
    mv conftest /usr/local/bin && \
    echo "OPA Conftest: $(conftest --version)"

# Do some clean up
RUN rm -f ./get_helm.sh ./pluto_*_linux_amd64.tar.gz conftest_*_Linux_x86_64.tar.gz && \
    yum clean all

CMD ["/bin/bash"]